using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace PhotoFX
{
	/// <summary>
	/// Summary description for dlgChannel.
	/// </summary>
	public class dlgChannel : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.GroupBox groupBox1;
		public System.Windows.Forms.CheckBox chkRed;
		public System.Windows.Forms.CheckBox chkGreen;
		public System.Windows.Forms.CheckBox chkBlue;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public dlgChannel()
		{
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdOK = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.chkRed = new System.Windows.Forms.CheckBox();
			this.chkGreen = new System.Windows.Forms.CheckBox();
			this.chkBlue = new System.Windows.Forms.CheckBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// cmdOK
			// 
			this.cmdOK.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cmdOK.Location = new System.Drawing.Point(92, 124);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.TabIndex = 0;
			this.cmdOK.Text = "&OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cmdCancel.Location = new System.Drawing.Point(8, 124);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.TabIndex = 0;
			this.cmdCancel.Text = "&Cancel";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.chkRed,
																					this.chkGreen,
																					this.chkBlue});
			this.groupBox1.Location = new System.Drawing.Point(8, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(160, 112);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Channels";
			// 
			// chkRed
			// 
			this.chkRed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkRed.Location = new System.Drawing.Point(16, 24);
			this.chkRed.Name = "chkRed";
			this.chkRed.Size = new System.Drawing.Size(124, 16);
			this.chkRed.TabIndex = 2;
			this.chkRed.Text = "Red channel";
			// 
			// chkGreen
			// 
			this.chkGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkGreen.Location = new System.Drawing.Point(16, 52);
			this.chkGreen.Name = "chkGreen";
			this.chkGreen.Size = new System.Drawing.Size(124, 16);
			this.chkGreen.TabIndex = 2;
			this.chkGreen.Text = "Green channel";
			// 
			// chkBlue
			// 
			this.chkBlue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkBlue.Location = new System.Drawing.Point(16, 80);
			this.chkBlue.Name = "chkBlue";
			this.chkBlue.Size = new System.Drawing.Size(120, 16);
			this.chkBlue.TabIndex = 2;
			this.chkBlue.Text = "Blue channel";
			// 
			// dlgChannel
			// 
			this.AcceptButton = this.cmdOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cmdCancel;
			this.ClientSize = new System.Drawing.Size(176, 151);
			this.ControlBox = false;
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.groupBox1,
																		  this.cmdOK,
																		  this.cmdCancel});
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "dlgChannel";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Extract Channel";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void cmdOK_Click(object sender, System.EventArgs e) {
			if (chkRed.Checked && chkGreen.Checked && chkBlue.Checked) {
				MessageBox.Show("With all channels selected there\nwill be no change to the original picture.", "Warning");
				return;
			} else if (!chkRed.Checked && !chkGreen.Checked && !chkBlue.Checked) {
				 MessageBox.Show("With no channels selected, picture would be black.", "Warning");
				 return;
			}
			DialogResult = DialogResult.OK;
		}
	}
}
