using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace PhotoFX
{
	/// <summary>
	/// Summary description for dlgValue.
	/// </summary>
	public class dlgValue : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		public System.Windows.Forms.NumericUpDown number;
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Button cmdCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public dlgValue(string caption, int min, int max, int def, bool dec)
		{
			InitializeComponent();
			this.Text = caption;
			number.Minimum = min;
			number.Maximum = max;
			number.Value = def;
			number.DecimalPlaces = dec ? 1 : 0;
		}
		public dlgValue(string caption, int min, int max, int def)
		{
			InitializeComponent();
			this.Text = caption;
			number.Minimum = min;
			number.Maximum = max;
			number.Value = def;
		}
		public dlgValue(string caption, int min, int max)
		{
			InitializeComponent();
			number.Minimum = min;
			number.Maximum = max;
			number.Value = (int)((max + min) / 2);
		}
		public dlgValue(int min, int max)
		{
			InitializeComponent();
			number.Minimum = min;
			number.Maximum = max;
			number.Value = (int)((max + min) / 2);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )	{
			if( disposing )	{
				if(components != null)	{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.number = new System.Windows.Forms.NumericUpDown();
			this.cmdOK = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.number)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.number});
			this.groupBox1.Location = new System.Drawing.Point(8, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(156, 68);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Select amount";
			// 
			// number
			// 
			this.number.Location = new System.Drawing.Point(16, 28);
			this.number.Name = "number";
			this.number.Size = new System.Drawing.Size(124, 20);
			this.number.TabIndex = 1;
			this.number.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.number.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
			this.number.Validating += new System.ComponentModel.CancelEventHandler(this.number_Validating);
			// 
			// cmdOK
			// 
			this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cmdOK.Location = new System.Drawing.Point(88, 84);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.TabIndex = 1;
			this.cmdOK.Text = "&OK";
			// 
			// cmdCancel
			// 
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cmdCancel.Location = new System.Drawing.Point(8, 84);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.TabIndex = 1;
			this.cmdCancel.Text = "&Cancel";
			// 
			// dlgValue
			// 
			this.AcceptButton = this.cmdOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cmdCancel;
			this.ClientSize = new System.Drawing.Size(172, 115);
			this.ControlBox = false;
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.cmdOK,
																		  this.groupBox1,
																		  this.cmdCancel});
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "dlgValue";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Amount";
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.number)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void number_Validating(object sender, System.ComponentModel.CancelEventArgs e) {
			if (number.Value < number.Minimum || number.Value > number.Maximum)
				e.Cancel = true;
		}
	}
}
