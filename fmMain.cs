using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace PhotoFX
{
	/// <summary>
	/// Summary description for fmMain.
	/// </summary>
	public class fmMain : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Panel canvas;

		protected bool modified = false;
		private Bitmap buffer = null;
		private Bitmap undobuf = null;

		#region Object Variables
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuFile;
		private System.Windows.Forms.MenuItem menuFile_Open;
		private System.Windows.Forms.MenuItem menuFilters;
		private System.Windows.Forms.MenuItem menuFilters_Colors;
		private System.Windows.Forms.MenuItem menuFilters_Colors_Gray;
		private System.Windows.Forms.MenuItem menuFilters_Transform;
		private System.Windows.Forms.MenuItem menuFilters_Transform_FlipX;
		private System.Windows.Forms.MenuItem menuFilters_Transform_FlipY;
		private System.Windows.Forms.MenuItem menuFilters_Colors_Invert;
		private System.Windows.Forms.MenuItem menuFilters_Colors_Solarize;
		private System.Windows.Forms.Label lblProcessing;
		private System.Windows.Forms.MenuItem menuFilters_Channel;
		private System.Windows.Forms.MenuItem menuFilters_Effects;
		private System.Windows.Forms.MenuItem menuFilters_Effects_Blur;
		private System.Windows.Forms.MenuItem menuFilters_Effects_Chrome;
		private System.Windows.Forms.MenuItem menuFilters_Edges_TraceEdge;
		private System.Windows.Forms.MenuItem menuFilters_Edges;
		private System.Windows.Forms.MenuItem menuFilters_Edges_ExtractEdge;
		private System.Windows.Forms.MenuItem menuFilters_Edges_HighEdge;
		private System.Windows.Forms.MenuItem menuFilters_Colors_BnW;
		private System.Windows.Forms.MenuItem menuEdit;
		private System.Windows.Forms.MenuItem menuFilters_Effects_Overexpose;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuFilters_Effects_Lighten;
		private System.Windows.Forms.MenuItem menuFilters_Effects_Darken;
		private System.Windows.Forms.MenuItem menuFilters_Effects_washout;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuFilters_Mix_Halfcolor;
		private System.Windows.Forms.MenuItem menuFilters_Mix_Lightmap;
		private System.Windows.Forms.MenuItem menuFilters_Mix_InterlaceX;
		private System.Windows.Forms.MenuItem menuFilters_Mix_InterlaceY;
		private System.Windows.Forms.MenuItem menuFilters_Mix_Screen;
		private System.Windows.Forms.MenuItem menuFilters_Mix_Multiply;
		private System.Windows.Forms.MenuItem menuFile_SaveAs;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem mnuFilters_Twist;
		private System.Windows.Forms.MenuItem menuEdit_Undo;
		#endregion

		public fmMain()	{
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ){
			if (buffer != null)
				buffer.Dispose();
			if( disposing ){
				if(components != null){
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(fmMain));
			this.canvas = new System.Windows.Forms.Panel();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuFile = new System.Windows.Forms.MenuItem();
			this.menuFile_Open = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuFile_SaveAs = new System.Windows.Forms.MenuItem();
			this.menuEdit = new System.Windows.Forms.MenuItem();
			this.menuEdit_Undo = new System.Windows.Forms.MenuItem();
			this.menuFilters = new System.Windows.Forms.MenuItem();
			this.menuFilters_Transform = new System.Windows.Forms.MenuItem();
			this.menuFilters_Transform_FlipX = new System.Windows.Forms.MenuItem();
			this.menuFilters_Transform_FlipY = new System.Windows.Forms.MenuItem();
			this.menuFilters_Colors = new System.Windows.Forms.MenuItem();
			this.menuFilters_Colors_Invert = new System.Windows.Forms.MenuItem();
			this.menuFilters_Colors_Gray = new System.Windows.Forms.MenuItem();
			this.menuFilters_Colors_Solarize = new System.Windows.Forms.MenuItem();
			this.menuFilters_Channel = new System.Windows.Forms.MenuItem();
			this.menuFilters_Colors_BnW = new System.Windows.Forms.MenuItem();
			this.menuFilters_Effects = new System.Windows.Forms.MenuItem();
			this.menuFilters_Effects_Blur = new System.Windows.Forms.MenuItem();
			this.menuFilters_Effects_Chrome = new System.Windows.Forms.MenuItem();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuFilters_Effects_Lighten = new System.Windows.Forms.MenuItem();
			this.menuFilters_Effects_Darken = new System.Windows.Forms.MenuItem();
			this.menuFilters_Effects_Overexpose = new System.Windows.Forms.MenuItem();
			this.menuFilters_Effects_washout = new System.Windows.Forms.MenuItem();
			this.menuFilters_Edges = new System.Windows.Forms.MenuItem();
			this.menuFilters_Edges_TraceEdge = new System.Windows.Forms.MenuItem();
			this.menuFilters_Edges_ExtractEdge = new System.Windows.Forms.MenuItem();
			this.menuFilters_Edges_HighEdge = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuFilters_Mix_Halfcolor = new System.Windows.Forms.MenuItem();
			this.menuFilters_Mix_Lightmap = new System.Windows.Forms.MenuItem();
			this.menuFilters_Mix_InterlaceX = new System.Windows.Forms.MenuItem();
			this.menuFilters_Mix_InterlaceY = new System.Windows.Forms.MenuItem();
			this.menuFilters_Mix_Screen = new System.Windows.Forms.MenuItem();
			this.menuFilters_Mix_Multiply = new System.Windows.Forms.MenuItem();
			this.lblProcessing = new System.Windows.Forms.Label();
			this.mnuFilters_Twist = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// canvas
			// 
			this.canvas.Name = "canvas";
			this.canvas.Size = new System.Drawing.Size(100, 100);
			this.canvas.TabIndex = 0;
			this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuFile,
																					  this.menuEdit,
																					  this.menuFilters});
			// 
			// menuFile
			// 
			this.menuFile.Index = 0;
			this.menuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuFile_Open,
																					 this.menuItem5,
																					 this.menuFile_SaveAs});
			this.menuFile.Text = "&File";
			// 
			// menuFile_Open
			// 
			this.menuFile_Open.Index = 0;
			this.menuFile_Open.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
			this.menuFile_Open.Text = "&Open...";
			this.menuFile_Open.Click += new System.EventHandler(this.menuFile_Open_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 1;
			this.menuItem5.Text = "-";
			// 
			// menuFile_SaveAs
			// 
			this.menuFile_SaveAs.Index = 2;
			this.menuFile_SaveAs.Text = "Save &As...";
			this.menuFile_SaveAs.Click += new System.EventHandler(this.menuFile_SaveAs_Click);
			// 
			// menuEdit
			// 
			this.menuEdit.Index = 1;
			this.menuEdit.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuEdit_Undo});
			this.menuEdit.Text = "&Edit";
			this.menuEdit.Popup += new System.EventHandler(this.menuEdit_Popup);
			// 
			// menuEdit_Undo
			// 
			this.menuEdit_Undo.Index = 0;
			this.menuEdit_Undo.Shortcut = System.Windows.Forms.Shortcut.CtrlZ;
			this.menuEdit_Undo.Text = "&Undo";
			this.menuEdit_Undo.Click += new System.EventHandler(this.menuEdit_Undo_Click);
			// 
			// menuFilters
			// 
			this.menuFilters.Enabled = false;
			this.menuFilters.Index = 2;
			this.menuFilters.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.menuFilters_Transform,
																						this.menuFilters_Colors,
																						this.menuFilters_Effects,
																						this.menuFilters_Edges,
																						this.menuItem2});
			this.menuFilters.Text = "F&ilters";
			// 
			// menuFilters_Transform
			// 
			this.menuFilters_Transform.Index = 0;
			this.menuFilters_Transform.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																								  this.menuFilters_Transform_FlipX,
																								  this.menuFilters_Transform_FlipY,
																								  this.mnuFilters_Twist});
			this.menuFilters_Transform.Text = "Transform";
			// 
			// menuFilters_Transform_FlipX
			// 
			this.menuFilters_Transform_FlipX.Index = 0;
			this.menuFilters_Transform_FlipX.Shortcut = System.Windows.Forms.Shortcut.CtrlK;
			this.menuFilters_Transform_FlipX.Text = "Flip &X";
			this.menuFilters_Transform_FlipX.Click += new System.EventHandler(this.menuFilters_Transform_FlipX_Click);
			// 
			// menuFilters_Transform_FlipY
			// 
			this.menuFilters_Transform_FlipY.Index = 1;
			this.menuFilters_Transform_FlipY.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
			this.menuFilters_Transform_FlipY.Text = "Flip &Y";
			this.menuFilters_Transform_FlipY.Click += new System.EventHandler(this.menuFilters_Transform_FlipY_Click);
			// 
			// menuFilters_Colors
			// 
			this.menuFilters_Colors.Index = 1;
			this.menuFilters_Colors.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							   this.menuFilters_Colors_Invert,
																							   this.menuFilters_Colors_Gray,
																							   this.menuFilters_Colors_Solarize,
																							   this.menuFilters_Channel,
																							   this.menuFilters_Colors_BnW});
			this.menuFilters_Colors.Text = "&Colors";
			// 
			// menuFilters_Colors_Invert
			// 
			this.menuFilters_Colors_Invert.Index = 0;
			this.menuFilters_Colors_Invert.Shortcut = System.Windows.Forms.Shortcut.CtrlI;
			this.menuFilters_Colors_Invert.Text = "&Invert";
			this.menuFilters_Colors_Invert.Click += new System.EventHandler(this.menuFilters_Colors_Invert_Click);
			// 
			// menuFilters_Colors_Gray
			// 
			this.menuFilters_Colors_Gray.Index = 1;
			this.menuFilters_Colors_Gray.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftG;
			this.menuFilters_Colors_Gray.Text = "Grayscale";
			this.menuFilters_Colors_Gray.Click += new System.EventHandler(this.menuFilters_Colors_Gray_Click);
			// 
			// menuFilters_Colors_Solarize
			// 
			this.menuFilters_Colors_Solarize.Index = 2;
			this.menuFilters_Colors_Solarize.Text = "&Solarize...";
			this.menuFilters_Colors_Solarize.Click += new System.EventHandler(this.menuFilters_Colors_Solarize_Click);
			// 
			// menuFilters_Channel
			// 
			this.menuFilters_Channel.Index = 3;
			this.menuFilters_Channel.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftC;
			this.menuFilters_Channel.Text = "Cha&nnels...";
			this.menuFilters_Channel.Click += new System.EventHandler(this.menuFilters_Channel_Click);
			// 
			// menuFilters_Colors_BnW
			// 
			this.menuFilters_Colors_BnW.Index = 4;
			this.menuFilters_Colors_BnW.Text = "&Black && White";
			this.menuFilters_Colors_BnW.Click += new System.EventHandler(this.menuFilters_Colors_BnW_Click);
			// 
			// menuFilters_Effects
			// 
			this.menuFilters_Effects.Index = 2;
			this.menuFilters_Effects.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																								this.menuFilters_Effects_Blur,
																								this.menuFilters_Effects_Chrome,
																								this.menuItem1,
																								this.menuFilters_Effects_Lighten,
																								this.menuFilters_Effects_Darken,
																								this.menuFilters_Effects_Overexpose,
																								this.menuFilters_Effects_washout});
			this.menuFilters_Effects.Text = "&Effects";
			// 
			// menuFilters_Effects_Blur
			// 
			this.menuFilters_Effects_Blur.Index = 0;
			this.menuFilters_Effects_Blur.Shortcut = System.Windows.Forms.Shortcut.CtrlB;
			this.menuFilters_Effects_Blur.Text = "&Blur";
			this.menuFilters_Effects_Blur.Click += new System.EventHandler(this.menuFilters_Effects_Blur_Click);
			// 
			// menuFilters_Effects_Chrome
			// 
			this.menuFilters_Effects_Chrome.Index = 1;
			this.menuFilters_Effects_Chrome.Text = "&Chrome";
			this.menuFilters_Effects_Chrome.Click += new System.EventHandler(this.menuFilters_Effects_Chrome_Click);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 2;
			this.menuItem1.Text = "-";
			// 
			// menuFilters_Effects_Lighten
			// 
			this.menuFilters_Effects_Lighten.Index = 3;
			this.menuFilters_Effects_Lighten.Shortcut = System.Windows.Forms.Shortcut.Alt1;
			this.menuFilters_Effects_Lighten.Text = "&Lighten";
			this.menuFilters_Effects_Lighten.Click += new System.EventHandler(this.menuFilters_Effects_Lighten_Click);
			// 
			// menuFilters_Effects_Darken
			// 
			this.menuFilters_Effects_Darken.Index = 4;
			this.menuFilters_Effects_Darken.Shortcut = System.Windows.Forms.Shortcut.Alt2;
			this.menuFilters_Effects_Darken.Text = "&Darken";
			this.menuFilters_Effects_Darken.Click += new System.EventHandler(this.menuFilters_Effects_Darken_Click);
			// 
			// menuFilters_Effects_Overexpose
			// 
			this.menuFilters_Effects_Overexpose.Index = 5;
			this.menuFilters_Effects_Overexpose.Text = "&Overexpose";
			this.menuFilters_Effects_Overexpose.Click += new System.EventHandler(this.menuFilters_Effects_Overexpose_Click);
			// 
			// menuFilters_Effects_washout
			// 
			this.menuFilters_Effects_washout.Index = 6;
			this.menuFilters_Effects_washout.Text = "&Wash out";
			this.menuFilters_Effects_washout.Click += new System.EventHandler(this.menuFilters_Effects_washout_Click);
			// 
			// menuFilters_Edges
			// 
			this.menuFilters_Edges.Index = 3;
			this.menuFilters_Edges.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							  this.menuFilters_Edges_TraceEdge,
																							  this.menuFilters_Edges_ExtractEdge,
																							  this.menuFilters_Edges_HighEdge});
			this.menuFilters_Edges.Text = "E&dges";
			// 
			// menuFilters_Edges_TraceEdge
			// 
			this.menuFilters_Edges_TraceEdge.Index = 0;
			this.menuFilters_Edges_TraceEdge.Text = "&Trace Edges";
			this.menuFilters_Edges_TraceEdge.Click += new System.EventHandler(this.menuFilters_Edges_TraceEdge_Click);
			// 
			// menuFilters_Edges_ExtractEdge
			// 
			this.menuFilters_Edges_ExtractEdge.Index = 1;
			this.menuFilters_Edges_ExtractEdge.Text = "&Extract Edges";
			this.menuFilters_Edges_ExtractEdge.Click += new System.EventHandler(this.menuFilters_Edges_ExtractEdge_Click);
			// 
			// menuFilters_Edges_HighEdge
			// 
			this.menuFilters_Edges_HighEdge.Index = 2;
			this.menuFilters_Edges_HighEdge.Text = "&Highlight Edges";
			this.menuFilters_Edges_HighEdge.Click += new System.EventHandler(this.menuFilters_Edges_HighEdge_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 4;
			this.menuItem2.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuFilters_Mix_Halfcolor,
																					  this.menuFilters_Mix_Lightmap,
																					  this.menuFilters_Mix_InterlaceX,
																					  this.menuFilters_Mix_InterlaceY,
																					  this.menuFilters_Mix_Screen,
																					  this.menuFilters_Mix_Multiply});
			this.menuItem2.Text = "&Mix";
			// 
			// menuFilters_Mix_Halfcolor
			// 
			this.menuFilters_Mix_Halfcolor.Index = 0;
			this.menuFilters_Mix_Halfcolor.Text = "&Halfcolor";
			this.menuFilters_Mix_Halfcolor.Click += new System.EventHandler(this.menuFilters_Mix_Halfcolor_Click);
			// 
			// menuFilters_Mix_Lightmap
			// 
			this.menuFilters_Mix_Lightmap.Index = 1;
			this.menuFilters_Mix_Lightmap.Text = "&Lightmap";
			this.menuFilters_Mix_Lightmap.Click += new System.EventHandler(this.menuFilters_Mix_Lightmap_Click);
			// 
			// menuFilters_Mix_InterlaceX
			// 
			this.menuFilters_Mix_InterlaceX.Index = 2;
			this.menuFilters_Mix_InterlaceX.Text = "Interlace &X";
			this.menuFilters_Mix_InterlaceX.Click += new System.EventHandler(this.menuFilters_Mix_InterlaceX_Click);
			// 
			// menuFilters_Mix_InterlaceY
			// 
			this.menuFilters_Mix_InterlaceY.Index = 3;
			this.menuFilters_Mix_InterlaceY.Text = "Interlace &Y";
			this.menuFilters_Mix_InterlaceY.Click += new System.EventHandler(this.menuFilters_Mix_InterlaceY_Click);
			// 
			// menuFilters_Mix_Screen
			// 
			this.menuFilters_Mix_Screen.Index = 4;
			this.menuFilters_Mix_Screen.Text = "&Screen";
			this.menuFilters_Mix_Screen.Click += new System.EventHandler(this.menuFilters_Mix_Screen_Click);
			// 
			// menuFilters_Mix_Multiply
			// 
			this.menuFilters_Mix_Multiply.Index = 5;
			this.menuFilters_Mix_Multiply.Text = "&Multiply";
			this.menuFilters_Mix_Multiply.Click += new System.EventHandler(this.menuFilters_Mix_Multiply_Click);
			// 
			// lblProcessing
			// 
			this.lblProcessing.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.lblProcessing.BackColor = System.Drawing.Color.White;
			this.lblProcessing.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblProcessing.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblProcessing.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.lblProcessing.Location = new System.Drawing.Point(118, 139);
			this.lblProcessing.Name = "lblProcessing";
			this.lblProcessing.Size = new System.Drawing.Size(156, 22);
			this.lblProcessing.TabIndex = 1;
			this.lblProcessing.Text = "PROCESSING...";
			this.lblProcessing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.lblProcessing.Visible = false;
			// 
			// mnuFilters_Twist
			// 
			this.mnuFilters_Twist.Index = 2;
			this.mnuFilters_Twist.Text = "&Twist";
			this.mnuFilters_Twist.Click += new System.EventHandler(this.mnuFilters_Twist_Click);
			// 
			// fmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(392, 300);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lblProcessing,
																		  this.canvas});
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Menu = this.mainMenu1;
			this.Name = "fmMain";
			this.Text = "Viperine PhotoFX 0.9";
			this.ResumeLayout(false);

		}
		#endregion
		
		[STAThread]
		static void Main() {
			Application.Run(new fmMain());
		}
		
		private void menuFile_Open_Click(object sender, System.EventArgs e) {
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.Filter = "Images|*.bmp; *.gif; *.jpg; *.jpeg; *.png|All files|*.*";
			if (ofd.ShowDialog() == DialogResult.OK) {
				buffer = new Bitmap(ofd.FileName);
				canvas.Size = new Size(buffer.Width, buffer.Height);
				Rectangle sr = Screen.FromHandle(this.Handle).Bounds;
				if (buffer.Width < sr.Width - 50 - Left && buffer.Height < sr.Height - 50 - Top) {
					ClientSize = new Size(buffer.Width, buffer.Height);
				}
				canvas.Invalidate();
				modified = false;
				menuFilters.Enabled = true;
			}
		}
		
		private void canvas_Paint(object sender, System.Windows.Forms.PaintEventArgs e) {
			if (buffer == null)
				return;
			e.Graphics.DrawImage(buffer, canvas.ClientRectangle);
		}
		
		private void dispError(Exception exc) {
			MessageBox.Show("An error occurred.\n\n" + exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			this.Enabled = true;
		}
		private void process(bool start) {
			lblProcessing.Visible = start;
			this.Enabled = !start;
			if (start) {
				undobuf	= buffer;
				// show something
			} else {
				// stop showin'
				modified = true;
				canvas.Invalidate();
			}
		}
		
		private float getAmount(string caption, int min, int max, int def, bool dec) {
			dlgValue dv = new dlgValue(caption, min, max, def, dec);
			float v;
			if (dv.ShowDialog() == DialogResult.OK)
				v = (float)dv.number.Value;
			else
				v = -1;
			dv.Dispose();
			return v;
		}
		private int getAmount(string caption, int min, int max, int def) {
			dlgValue dv = new dlgValue(caption, min, max, def);
			int v;
			if (dv.ShowDialog() == DialogResult.OK)
				v = (int)dv.number.Value;
			else
				v = -1;
			dv.Dispose();
			return v;
		}
		private int getAmount(string caption, int max, int def) {
			dlgValue dv = new dlgValue(caption, max, def);
			int v;
			if (dv.ShowDialog() == DialogResult.OK)
				v = (int)dv.number.Value;
			else
				v = -1;
			dv.Dispose();
			return v;
		}
		private int getAmount(int min, int max) {
			dlgValue dv = new dlgValue(min, max);
			int v;
			if (dv.ShowDialog() == DialogResult.OK)
				v = (int)dv.number.Value;
			else
				v = -1;
			dv.Dispose();
			return v;
		}

		private Bitmap getImage() {
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.Filter = "Image files|*.bmp; *.jpg; ;*.jpeg *.gif; *.png|All files|*.*";
			if (ofd.ShowDialog() == DialogResult.OK)
				return new Bitmap(ofd.FileName);
			else return null;
		}
		
		private void menuEdit_Undo_Click(object sender, System.EventArgs e) {
			buffer = undobuf;
			undobuf = null;
			canvas.Invalidate();
		}
		private void menuEdit_Popup(object sender, System.EventArgs e) {
			menuEdit_Undo.Enabled = (undobuf != null);
		}
		
		/**
		 * FILTERS AND EFFECTS
		 */
		
		private void menuFilters_Colors_Gray_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc;
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, 
							Color.FromArgb((int)((pc.R + pc.G + pc.B)/3),(int)((pc.R + pc.G + pc.B)/3),(int)((pc.R + pc.G + pc.B)/3)));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}
		
		private void menuFilters_Transform_FlipX_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						img.SetPixel(x, y, buffer.GetPixel(buffer.Width - x - 1, y));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}
		
		private void menuFilters_Transform_FlipY_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						img.SetPixel(x, y, buffer.GetPixel(x, buffer.Height - y - 1));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void mnuFilters_Twist_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				int disl;
				for (int y = 0; y < buffer.Height; y++) {
					for (int x = 0; x < buffer.Width; x++) {
						disl = x - y;
						if (disl < 0) disl = buffer.Width + disl;
						img.SetPixel(x, y, buffer.GetPixel(disl, y));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}
		
		private void menuFilters_Colors_Invert_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc;
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, 
							Color.FromArgb(255 - pc.R, 255 - pc.G, 255 - pc.B));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Colors_Solarize_Click(object sender, System.EventArgs e) {
			try {
				// call the dialog
				dlgSolarize ds = new dlgSolarize();
				if (ds.ShowDialog() == DialogResult.Cancel) return;
				byte tr = (byte)ds.tred.Value;
				byte tg = (byte)ds.tgreen.Value;
				byte tb = (byte)ds.tblue.Value;
				bool screen = ds.radScreen.Checked;
				ds.Dispose();
				
				process(true);
				Color pc;
				byte r = 0, g = 0, b = 0;
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						if (screen) {
							if (pc.R < tr) r = (byte)(255 - pc.R); else r = pc.R;
							if (pc.G < tg) g = (byte)(255 - pc.G); else g = pc.G;
							if (pc.B < tb) b = (byte)(255 - pc.B); else b = pc.B;
						} else {
							if (pc.R > tr) r = (byte)(255 - pc.R); else r = pc.R;
							if (pc.G > tg) g = (byte)(255 - pc.G); else g = pc.G;
							if (pc.B > tb) b = (byte)(255 - pc.B); else b = pc.B;
						}
						img.SetPixel(x, y, Color.FromArgb(r, g, b));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Channel_Click(object sender, System.EventArgs e) {
			try {
				// call the dialog
				dlgChannel dc = new dlgChannel();
				if (dc.ShowDialog() == DialogResult.Cancel) return;
				bool cr = dc.chkRed.Checked;
				bool cg = dc.chkGreen.Checked;
				bool cb = dc.chkBlue.Checked;
				dc.Dispose();
				
				process(true);
				Color pc;
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, Color.FromArgb(cr ? pc.R : (byte)0, cg ? pc.G : (byte)0, cb ? pc.B : (byte)0));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Effects_Blur_Click(object sender, System.EventArgs e) {
			try {
				process(true);
				Color pc;
				Color pcX, pcY;
				
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width-1; x++) {
					for (int y = 0; y < buffer.Height-1; y++) {
						pc = buffer.GetPixel(x, y);
						pcX = buffer.GetPixel(x+1, y);
						pcY = buffer.GetPixel(x, y+1);
						img.SetPixel(x, y, Color.FromArgb(
								(pc.R + pcX.R + pcY.R) / 3,
								(pc.G + pcX.G + pcY.G) / 3,
								(pc.B + pcX.B + pcY.B) / 3
							));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Effects_Chrome_Click(object sender, System.EventArgs e) {
			try {
				process(true);
				Color pc;
				byte v, last = 0;
				
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width-1; x++) {
					for (int y = 0; y < buffer.Height-1; y++) {
						pc = buffer.GetPixel(x, y);
						v = (byte)((((pc.R + pc.G + pc.B) % 255) + last) / 2);
						img.SetPixel(x, y, Color.FromArgb(v, v, v));
						last = v;
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Edges_Main(byte mode) {
			try {
				process(true);
				Color pc;
				Color pcX, pcY;

				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width-1; x++) {
					for (int y = 0; y < buffer.Height-1; y++) {
						pc = buffer.GetPixel(x, y);
						pcX = buffer.GetPixel(x+1, y);
						pcY = buffer.GetPixel(x, y+1);
						if (Math.Abs((pc.R+pc.G+pc.B)-(pcX.R+pcX.G+pcX.B)) >= 64 || Math.Abs((pc.R+pc.G+pc.B)-(pcY.R+pcY.G+pcY.B)) >= 64)
							img.SetPixel(x, y, mode>0 ? Color.White : pc);
						else
							img.SetPixel(x, y, mode==2 ? pc : Color.Black);
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Edges_TraceEdge_Click(object sender, System.EventArgs e) {
			menuFilters_Edges_Main(0);
		}
		private void menuFilters_Edges_ExtractEdge_Click(object sender, System.EventArgs e) {
			menuFilters_Edges_Main(1);
		}
		private void menuFilters_Edges_HighEdge_Click(object sender, System.EventArgs e) {
			menuFilters_Edges_Main(2);
		}

		private void menuFilters_Colors_BnW_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				int limit = getAmount("Threshold", 0, 255);
				if (limit<0) { process(false); return; }
				Color pc;
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, 
							Color.FromArgb(pc.R < limit ? 0 : 255, pc.R < limit ? 0 : 255, pc.R < limit ? 0 : 255));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Effects_Lighten_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc;
				int a = getAmount("Amount", 1, 255, 32);
				if (a<0) { process(false); return; }
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, 
							Color.FromArgb(
							Math.Min(255, pc.R + a),
							Math.Min(255, pc.G + a),
							Math.Min(255, pc.B + a)
							));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Effects_Darken_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc;
				int a = getAmount("Amount", 1, 255, 32);
				if (a<0) { process(false); return; }
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, 
							Color.FromArgb(
							Math.Max(0, pc.R - a),
							Math.Max(0, pc.G - a),
							Math.Max(0, pc.B - a)
							));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Effects_Overexpose_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc;
				float m = getAmount("Multiplier", 1, 10, 2, true);
				if (m<0) { process(false); return; }
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, 
							Color.FromArgb(
								(int)Math.Max(0, Math.Min(255, pc.R * m - (m-1)*128)),
								(int)Math.Max(0, Math.Min(255, pc.G * m - (m-1)*128)),
								(int)Math.Max(0, Math.Min(255, pc.B * m - (m-1)*128))
							));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Effects_washout_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc;
				float m = getAmount("Multiplier", 1, 10, 2, true);
				if (m<0) { process(false); return; }
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, 
							Color.FromArgb(
							(int)Math.Max(0, Math.Min(255, pc.R / m + (m-1)*128)),
							(int)Math.Max(0, Math.Min(255, pc.G / m + (m-1)*128)),
							(int)Math.Max(0, Math.Min(255, pc.B / m + (m-1)*128))
							));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}
		
		private void menuFilters_Mix_Halfcolor_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc, pn;
				Bitmap mix = getImage();
				if (mix==null) { process(false); return; };
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						if (x < mix.Width && y < mix.Height)
							pn = mix.GetPixel(x, y);
						else
							pn = buffer.GetPixel(x, y);
							img.SetPixel(x, y, Color.FromArgb((pc.R + pn.R) / 2, (pc.R + pn.R) / 2, (pc.R + pn.R) / 2));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}
		
		private void menuFilters_Mix_Lightmap_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc, pn;
				float mod;
				Bitmap mix = getImage();
				if (mix==null) { process(false); return; };
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						if (x < mix.Width && y < mix.Height)
							pn = mix.GetPixel(x, y);
						else
							pn = buffer.GetPixel(x, y);
						mod = Math.Max(((pn.R + pn.G + pn.B) / 3), 1);
						img.SetPixel(x, y, Color.FromArgb(
							(int)((float)pc.R / 255f * mod),
							(int)((float)pc.G / 255f * mod),
							(int)((float)pc.B / 255f * mod)
						));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Mix_InterlaceX_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc;
				bool switcher = false;
				Bitmap mix = getImage();
				if (mix==null) { process(false); return; };
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						//pc = buffer.GetPixel(x, y);
						if (x < mix.Width && y < mix.Height && switcher)
							pc = mix.GetPixel(x, y);
						else
							pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, pc);
						switcher = !switcher;
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Mix_InterlaceY_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc;
				bool switcher = false;
				Bitmap mix = getImage();
				if (mix==null) { process(false); return; };
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						//pc = buffer.GetPixel(x, y);
						if (x < mix.Width && y < mix.Height && switcher)
							pc = mix.GetPixel(x, y);
						else
							pc = buffer.GetPixel(x, y);
						img.SetPixel(x, y, pc);
					}
					switcher = !switcher;
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Mix_Screen_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc, pn;
				Bitmap mix = getImage();
				if (mix==null) { process(false); return; };
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						if (x < mix.Width && y < mix.Height)
							pn = mix.GetPixel(x, y);
						else
							pn = buffer.GetPixel(x, y);
						img.SetPixel(x, y, Color.FromArgb(
							Math.Max(pc.R, pn.R), Math.Max(pc.G, pn.G), Math.Max(pc.B, pn.B)
						));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFilters_Mix_Multiply_Click(object sender, System.EventArgs e) {
			process(true);
			try {
				Color pc, pn;
				Bitmap mix = getImage();
				if (mix==null) { process(false); return; };
				Bitmap img = new Bitmap(buffer.Width, buffer.Height);
				for (int x = 0; x < buffer.Width; x++) {
					for (int y = 0; y < buffer.Height; y++) {
						pc = buffer.GetPixel(x, y);
						if (x < mix.Width && y < mix.Height)
							pn = mix.GetPixel(x, y);
						else
							pn = buffer.GetPixel(x, y);
						img.SetPixel(x, y, Color.FromArgb(
							Math.Min(pc.R, pn.R), Math.Min(pc.G, pn.G), Math.Min(pc.B, pn.B)
							));
					}
				}
				buffer = img;
			} catch (Exception exc) {
				dispError(exc);
			}
			process(false);
		}

		private void menuFile_SaveAs_Click(object sender, System.EventArgs e) {
			try {
				SaveFileDialog sfd = new SaveFileDialog();
				sfd.AddExtension = true;
				if (sfd.ShowDialog() == DialogResult.OK) {
					buffer.Save(sfd.FileName);
				}
			} catch (Exception exc) {
				MessageBox.Show(exc.Message);
			};
		}
	}
}