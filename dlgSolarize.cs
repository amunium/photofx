using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace PhotoFX
{
	/// <summary>
	/// Summary description for dlgSolarize.
	/// </summary>
	public class dlgSolarize : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.GroupBox groupBox1;
		public System.Windows.Forms.NumericUpDown tred;
		public System.Windows.Forms.NumericUpDown tgreen;
		public System.Windows.Forms.NumericUpDown tblue;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox chkLock;
		private System.Windows.Forms.GroupBox groupBox2;
		public System.Windows.Forms.RadioButton radScreen;
		public System.Windows.Forms.RadioButton radMulti;
		private System.Windows.Forms.CheckBox chkHex;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public dlgSolarize() {
			InitializeComponent();
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )	{
			if( disposing )	{
				if(components != null)	{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdOK = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tred = new System.Windows.Forms.NumericUpDown();
			this.tgreen = new System.Windows.Forms.NumericUpDown();
			this.tblue = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.chkLock = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radScreen = new System.Windows.Forms.RadioButton();
			this.radMulti = new System.Windows.Forms.RadioButton();
			this.chkHex = new System.Windows.Forms.CheckBox();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tred)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tgreen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tblue)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// cmdOK
			// 
			this.cmdOK.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cmdOK.Location = new System.Drawing.Point(192, 144);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.TabIndex = 0;
			this.cmdOK.Text = "&OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cmdCancel.Location = new System.Drawing.Point(108, 144);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.TabIndex = 0;
			this.cmdCancel.Text = "&Cancel";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.tred,
																					this.tgreen,
																					this.tblue,
																					this.label1,
																					this.label2,
																					this.label3});
			this.groupBox1.Location = new System.Drawing.Point(108, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(160, 132);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Thresholds";
			// 
			// tred
			// 
			this.tred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tred.Location = new System.Drawing.Point(80, 24);
			this.tred.Maximum = new System.Decimal(new int[] {
																 255,
																 0,
																 0,
																 0});
			this.tred.Name = "tred";
			this.tred.Size = new System.Drawing.Size(64, 20);
			this.tred.TabIndex = 0;
			this.tred.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tred.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
			this.tred.Value = new System.Decimal(new int[] {
															   128,
															   0,
															   0,
															   0});
			this.tred.Validating += new System.ComponentModel.CancelEventHandler(this.updown_Validating);
			this.tred.ValueChanged += new System.EventHandler(this.updown_ValueChanged);
			// 
			// tgreen
			// 
			this.tgreen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tgreen.Location = new System.Drawing.Point(80, 60);
			this.tgreen.Maximum = new System.Decimal(new int[] {
																   255,
																   0,
																   0,
																   0});
			this.tgreen.Name = "tgreen";
			this.tgreen.Size = new System.Drawing.Size(64, 20);
			this.tgreen.TabIndex = 0;
			this.tgreen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tgreen.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
			this.tgreen.Value = new System.Decimal(new int[] {
																 128,
																 0,
																 0,
																 0});
			this.tgreen.Validating += new System.ComponentModel.CancelEventHandler(this.updown_Validating);
			this.tgreen.ValueChanged += new System.EventHandler(this.updown_ValueChanged);
			// 
			// tblue
			// 
			this.tblue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tblue.Location = new System.Drawing.Point(80, 96);
			this.tblue.Maximum = new System.Decimal(new int[] {
																  255,
																  0,
																  0,
																  0});
			this.tblue.Name = "tblue";
			this.tblue.Size = new System.Drawing.Size(64, 20);
			this.tblue.TabIndex = 0;
			this.tblue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tblue.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
			this.tblue.Value = new System.Decimal(new int[] {
																128,
																0,
																0,
																0});
			this.tblue.Validating += new System.ComponentModel.CancelEventHandler(this.updown_Validating);
			this.tblue.ValueChanged += new System.EventHandler(this.updown_ValueChanged);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 28);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 12);
			this.label1.TabIndex = 2;
			this.label1.Text = "Red";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.Green;
			this.label2.Location = new System.Drawing.Point(16, 64);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 12);
			this.label2.TabIndex = 2;
			this.label2.Text = "Green";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.label3.Location = new System.Drawing.Point(16, 100);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 12);
			this.label3.TabIndex = 2;
			this.label3.Text = "Blue";
			// 
			// chkLock
			// 
			this.chkLock.Checked = true;
			this.chkLock.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkLock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkLock.Location = new System.Drawing.Point(8, 148);
			this.chkLock.Name = "chkLock";
			this.chkLock.Size = new System.Drawing.Size(88, 20);
			this.chkLock.TabIndex = 2;
			this.chkLock.Text = "Lock values";
			this.chkLock.CheckedChanged += new System.EventHandler(this.chkLock_CheckedChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.radScreen,
																					this.radMulti});
			this.groupBox2.Location = new System.Drawing.Point(8, 4);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(92, 96);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Method";
			// 
			// radScreen
			// 
			this.radScreen.Location = new System.Drawing.Point(12, 60);
			this.radScreen.Name = "radScreen";
			this.radScreen.Size = new System.Drawing.Size(64, 20);
			this.radScreen.TabIndex = 4;
			this.radScreen.Text = "Lighten";
			// 
			// radMulti
			// 
			this.radMulti.Checked = true;
			this.radMulti.Location = new System.Drawing.Point(12, 24);
			this.radMulti.Name = "radMulti";
			this.radMulti.Size = new System.Drawing.Size(64, 20);
			this.radMulti.TabIndex = 4;
			this.radMulti.TabStop = true;
			this.radMulti.Text = "Darken";
			// 
			// chkHex
			// 
			this.chkHex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkHex.Location = new System.Drawing.Point(8, 116);
			this.chkHex.Name = "chkHex";
			this.chkHex.Size = new System.Drawing.Size(88, 20);
			this.chkHex.TabIndex = 2;
			this.chkHex.Text = "Hexadecimal";
			this.chkHex.CheckedChanged += new System.EventHandler(this.chkHex_CheckedChanged);
			// 
			// dlgSolarize
			// 
			this.AcceptButton = this.cmdOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cmdCancel;
			this.ClientSize = new System.Drawing.Size(274, 173);
			this.ControlBox = false;
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.groupBox2,
																		  this.chkLock,
																		  this.groupBox1,
																		  this.cmdOK,
																		  this.cmdCancel,
																		  this.chkHex});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "dlgSolarize";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Solarize";
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tred)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tgreen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tblue)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void cmdOK_Click(object sender, System.EventArgs e) {
			DialogResult = DialogResult.OK;
		}

		private void updown_Validating(object sender, System.ComponentModel.CancelEventArgs e) {
			if (((NumericUpDown)sender).Value > 255 || ((NumericUpDown)sender).Value < 0) {
				e.Cancel = true;
			}
		}

		private void updown_ValueChanged(object sender, System.EventArgs e) {
			if (chkLock.Checked) {
				tred  .Value = ((NumericUpDown)sender).Value;
				tgreen.Value = ((NumericUpDown)sender).Value;
				tblue .Value = ((NumericUpDown)sender).Value;
			}
		}

		private void chkLock_CheckedChanged(object sender, System.EventArgs e) {
			updown_ValueChanged((object)tred, new EventArgs());
		}

		private void chkHex_CheckedChanged(object sender, System.EventArgs e) {
			tred  .Hexadecimal = ((CheckBox)sender).Checked;
			tgreen.Hexadecimal = ((CheckBox)sender).Checked;
			tblue .Hexadecimal = ((CheckBox)sender).Checked;
		}
	}
}
